﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class DamageOnTrigger : MonoBehaviour
{
    [SerializeField] private int _damageCaused = 1;

    private HealthManager _targetHealthManager;

    void OnTriggerEnter2D(Collider2D coll)
    {
        Debug.Log(gameObject.name + " triggered");
        _targetHealthManager = coll.GetComponent<HealthManager>();
        if (_targetHealthManager != null)
        {
            _targetHealthManager.Damage(_damageCaused);
        }
        _targetHealthManager = null;
    }
}
