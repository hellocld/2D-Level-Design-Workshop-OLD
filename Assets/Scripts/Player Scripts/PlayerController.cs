﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Physics))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] private float _walkForce = 2f;
    [SerializeField] private float _jumpForce = 50;
    [SerializeField] private Vector2 _damageThrowbackForce;
    private Physics _physics;
    private HealthManager _healthManager;
    private SpriteRenderer _spriteRenderer;

    private bool _canControl = true;

    private bool _damaged;
    public bool damaged
    {
        get { return _damaged; }
        private set { }
    }

    [SerializeField] private float _postDamageInvincibilityTime = 5f;
    
    void Start()
    {
        _physics = gameObject.GetComponent<Physics>();

        _healthManager = gameObject.GetComponent<HealthManager>();
        _healthManager.OnDamage(() => { StartCoroutine(DamagedCoroutine()); });
        _healthManager.OnDeath(PlayerDead);
        _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (_canControl) GetPlayerInput();
    }

    private void GetPlayerInput()
    {
        _physics.AddForce(new Vector2(Input.GetAxis("Horizontal") * _walkForce, 0));

        if (Input.GetButtonDown("Jump") && _physics.canJump)
        {
            _physics.AddForce(new Vector2(0, _jumpForce));
        }
    }

    private void PlayerDead()
    {
        CanControl(false);
    }

    public void CanControl(bool aControl)
    {
        _canControl = aControl;
    }

    IEnumerator DamagedCoroutine()
    {
        Debug.Log("OUCH");
        StartCoroutine(InvincibleCoroutine());
        _canControl = false;
        _damaged = true;
        if (!_spriteRenderer.flipX)
        {
            _physics.AddForce(_damageThrowbackForce.x * -1, _damageThrowbackForce.y);
        }
        else
        {
            _physics.AddForce(_damageThrowbackForce);
        }

        yield return new WaitForSeconds(1);
        _damaged = false;
        _canControl = true;
    }

    IEnumerator InvincibleCoroutine()
    {
        _healthManager._invincible = true;
        yield return new WaitForSeconds(_postDamageInvincibilityTime);
        _healthManager._invincible = false;
    }
}
