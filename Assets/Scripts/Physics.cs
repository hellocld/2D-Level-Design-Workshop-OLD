﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Physics : MonoBehaviour
{

    [SerializeField] private float _mass, _gravity;
    [SerializeField] private Vector2 _maxVelocity, _drag;
    [SerializeField] private ContactFilter2D _raycastFilter;

    private Vector2 _acceleration, _velocity;
    
    private RaycastHit2D[] _raycastHit2Darray = new RaycastHit2D[100];

    public Vector2 velocity
    {
        get { return _velocity; }
        private set { }
    }

    private bool _isGrounded;

    public bool isGrounded
    {
        get { return _isGrounded; }
        private set { }
    }

    public bool canJump
    {
        get { return (_isGrounded && _playerCollider2D.Cast(Vector2.up, _raycastFilter, _raycastHit2Darray, 0.1f) == 0); }
        private set { }
    }

    private Collider2D _playerCollider2D;

    public void AddForce(Vector2 aForce)
    {
        _acceleration.x += aForce.x/_mass;
        _acceleration.y += aForce.y/_mass;
    }

    public void AddForce(float x, float y)
    {
        AddForce(new Vector2(x, y));
    }

    // Object physics initialization
    void Start()
    {
        // Acquire the collider component for the object
        _playerCollider2D = gameObject.GetComponent<Collider2D>();

        // Initialize the acceleration Vector2
        _acceleration = Vector2.zero;
        _velocity = Vector2.zero;

        // Assume the object does not start on the ground
        _isGrounded = false;
    }

    // All physics code resides in FixedUpdate to ensure consistent behavior
    void FixedUpdate()
    {
        GroundCheck();
        // Apply gravity, if not grounded
        // TODO - move gravity value to game manager
        if (!_isGrounded)
            AddForce(new Vector2(0, -_gravity));
        
        _velocity += _acceleration;
        _velocity.x -= _velocity.x*_drag.x;
        _velocity.y -= _velocity.y * _drag.y;

        // Reset acceleration after applying it, to prevent buildup of force
        _acceleration = Vector2.zero;

        // Clamp the velocity to the bounds set by _maxVelocity
        _velocity.x = Mathf.Clamp(_velocity.x, -_maxVelocity.x, _maxVelocity.x);
        _velocity.y = Mathf.Clamp(_velocity.y, -_maxVelocity.y, _maxVelocity.y);

        _playerCollider2D.attachedRigidbody.velocity = _velocity;
    }

    private void GroundCheck()
    {
        if (_playerCollider2D.Cast(Vector2.down, _raycastHit2Darray, 0.05f) == 0)
        {
            _isGrounded = false;
        }
        else
        {
            _isGrounded = true;
            if (_velocity.y < 0) _velocity.y = 0;
        }
    }

    //Use this callback to resolve collisions with objects in the scene
    void OnCollisionEnter2D(Collision2D coll)
    {
        ContactPoint2D[] tContactPoint2Ds = new ContactPoint2D[100];
        for (int i = 0; i < coll.GetContacts(tContactPoint2Ds); i++)
        {
            // Check to see if ground surface was collided with

            if(Vector2.Dot(tContactPoint2Ds[i].normal, Vector2.up) > 0.8)
            {
                _isGrounded = true;
                if (_velocity.y < 0) _velocity.y = 0;
            }

            // Check if object hit the roof (e.g. player hitting their head)
            if (Vector2.Dot(tContactPoint2Ds[i].normal, Vector2.down) > 0.8)
            {
                Debug.Log("Hit your head");
                if (_velocity.y > 0) _velocity.y = 0;
            }

            if (tContactPoint2Ds[i].normal == Vector2.left && _velocity.x > 0)
            {
                _velocity.x = 0;
            }

            if (tContactPoint2Ds[i].normal == Vector2.right && _velocity.x < 0)
            {
                _velocity.x = 0;
            }

        }
    }

    public void Halt()
    {
        _acceleration = Vector2.zero;
        _velocity = Vector2.zero;
    }

}
